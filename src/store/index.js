import Vue from 'vue'
import Vuex from 'vuex'
import system from '../store/system.js'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function ( /* { ssrContext } */ ) {
  const Store = new Vuex.Store({
    modules: {
      system
    },
    plugins: [createPersistedState()]
  })

  return Store
}
