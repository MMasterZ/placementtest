const state = {
  placementStudentData: "",
  placementCourseData: ""
};

const mutations = {
  setUserData: (state, payload) => {
    //Set ค่า userData
    state.placementStudentData = payload;
  },
  setCourseData: (state, payload) => {
    //Set ค่า CourseData
    state.placementCourseData = payload;
  }
};

const actions = {
  setUserData: ({ commit }, payload) => {
    commit("setUserData", payload);
  },
  setCourseData: ({ commit }, payload) => {
    commit("setCourseData", payload);
  }
};

export default {
  state,
  mutations,
  actions
};
